<?php
add_action('wp_head', 'gco_veoveo_header', 1000);
function gco_veoveo_header()
{

    echo '<link rel="icon" href="' . get_field('favicon', 'option') . '" type="image/gif">';

    $veoveo = get_field('veoveo', 'option');
    $chen_ma_header = $veoveo['chen_ma_header'];

    if (!empty($chen_ma_header)) {
        echo $chen_ma_header;
    }
}
