<?php

/**
 * @block-slug  :   lth-categories-service
 * @block-output:   lth_categories_service_output
 * @block-attributes: get from attributes.php
 */

// filter for Frontend output.
add_filter('lazyblock/lth-categories-service/frontend_callback', 'lth_categories_service_output_fe', 10, 2);

if (!function_exists('lth_categories_service_output_fe')) :
    /**
     * Test Render Callback
     *
     * @param string $output - block output.
     * @param array  $attributes - block attributes.
     */
    function lth_categories_service_output_fe($output, $attributes)
    {
        ob_start();
?>
        <article class="lth-categories <?php echo $attributes['class']; ?>">
            <div class="module module_categories module_categories_service">
                <?php if ($attributes['title'] || $attributes['description'] || $attributes['categories']) : ?>
                    <div class="module_header title-box title-align-<?php echo $attributes['title_align']; ?>">
                        <?php if ($attributes['title']) : ?>
                            <h2 class="title">
                                <?php if ($attributes['title_url']) : ?>
                                    <a href="<?php echo esc_url($attributes['title_url']); ?>" title="">
                                    <?php endif; ?>
                                    <?php echo wpautop(esc_html($attributes['title'])); ?>
                                    <?php if ($attributes['title_url']) : ?>
                                    </a>
                                <?php endif; ?>
                            </h2>
                        <?php endif; ?>

                        <?php if ($attributes['description']) : ?>
                            <div class="infor">
                                <?php echo wpautop(esc_html($attributes['description'])); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <div class="module_content">
                    <div class="swiper swiper-slider swiper-categories" data-item="<?php echo $attributes['item']; ?>"
                     data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>"
                      data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>"
                       data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>"
                        data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>"
                         data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                        <?php foreach ($attributes['items'] as $inner) {
                            $term = get_term_by('id', $inner['item'], 'service_cat'); ?>
                            <div class="item">
                                <div class="post-box">
                                    <?php $thumbnail_id = get_woocommerce_term_meta($inner['item'], 'thumbnail_id', true);
                                    $image = wp_get_attachment_url($thumbnail_id);
                                    $url_cat = get_term_link($term->slug, 'service_cat');
                                    if ($image) { ?>
                                        <div class="post-image">
                                            <a href="<?php if ($url_cat) {echo $url_cat;} ?>">
                                                <img src="<?php echo $image; ?>" alt="<?php echo $term->name; ?>" width="auto" height="auto">
                                            </a>
                                        </div>
                                    <?php } ?>

                                    <div class="post-content" style="text-align: <?php echo $attributes['text_align']; ?>">
                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                            <h1 class="post-name">
                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                            <h2 class="post-name">
                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                            <h3 class="post-name">
                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                            <h4 class="post-name">
                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                            <h5 class="post-name">
                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                            <h6 class="post-name">
                                        <?php } ?>
                                            <a href="<?php if ($url_cat) {echo $url_cat;} ?>">
                                                <?php echo $term->name; ?>
                                            </a>
                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                            </h1>
                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                            </h2>
                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                            </h3>
                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                            </h4>
                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                            </h5>
                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                            </h6>
                                        <?php } ?> 

                                        <?php if ($term->description) { ?>
                                            <div class="post-excerpt">
                                                <?php echo wpautop($term->description); ?>
                                            </div>
                                        <?php } ?>

                                        <div class="post-button" style="text-align: <?php echo $attributes['text_align']; ?>">
                                            <a href="<?php if ($url_cat) {echo $url_cat;} ?>" title="" class="btn"><?php echo __('Xem thêm'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </article>
<?php
        return ob_get_clean();
    }
endif;
?>