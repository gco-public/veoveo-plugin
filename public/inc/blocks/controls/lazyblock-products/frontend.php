<?php

/**
 * @block-slug  :   lth-products
 * @block-output:   lth_products_output
 * @block-attributes: get from attributes.php
 */

// filter for Frontend output.
add_filter('lazyblock/lth-products/frontend_callback', 'lth_products_output_fe', 10, 2);

if (!function_exists('lth_products_output_fe')) :
    /**
     * Test Render Callback
     *
     * @param string $output - block output.
     * @param array  $attributes - block attributes.
     */
    function lth_products_output_fe($output, $attributes)
    {
        ob_start();
?>
        <article class="lth-products">
            <div class="module module_products">
                <?php if ($attributes['title'] || $attributes['description'] || $attributes['show_items']) : ?>
                    <div class="module_header title-box title-align-<?php echo $attributes['title_align']; ?>">
                        <?php if ($attributes['title']) : ?>
                            <h2 class="title">
                                <?php if ($attributes['title_url']) : ?>
                                    <a href="<?php echo esc_url($attributes['title_url']); ?>" title="">
                                    <?php else : ?>
                                        <span>
                                        <?php endif; ?>
                                        <?php echo esc_html($attributes['title']); ?>
                                        <?php if ($attributes['title_url']) : ?>
                                    </a>
                                <?php else : ?>
                                    </span>
                                <?php endif; ?>
                            </h2>
                        <?php endif; ?>

                        <?php if ($attributes['description']) : ?>
                            <div class="infor">
                                <?php echo esc_html($attributes['description']); ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($attributes['show_items']) : ?>
                            <div class="cat-list tab-list">
                                <ul>
                                    <?php $j = 0;
                                    foreach ($attributes['select'] as $inner) :
                                        $j++;

                                        if ($inner == 'new_products') { ?>
                                            <li>
                                                <a href="#new_products" class="<?php if ($j == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                                    <?php echo __('Sản phẩm mới'); ?>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($inner == 'featured_products') { ?>
                                            <li>
                                                <a href="#featured_products" class="<?php if ($j == 1) {
                                                                                        echo 'active';
                                                                                    } ?>">
                                                    <?php echo __('Sản phẩm nổi bật'); ?>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($inner == 'sale_products') { ?>
                                            <li>
                                                <a href="#sale_products" class="<?php if ($j == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                                    <?php echo __('Sản phẩm giảm giá'); ?>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($inner == 'random_products') { ?>
                                            <li>
                                                <a href="#random_products" class="<?php if ($j == 1) {
                                                                                        echo 'active';
                                                                                    } ?>">
                                                    <?php echo __('Sản phẩm ngẫu nhiên'); ?>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($inner == 'viewed_products') { ?>
                                            <li>
                                                <a href="#viewed_products" class="<?php if ($j == 1) {
                                                                                        echo 'active';
                                                                                    } ?>">
                                                    <?php echo __('Sản phẩm vừa xem'); ?>
                                                </a>
                                            </li>
                                    <?php }
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <div class=" module_content content_text_<?php echo $attributes['text_align']; ?> tab-content module_style_<?php echo $attributes['style']; ?>">
                    <?php $i = 0;
                    foreach ($attributes['select'] as $inner) :
                        $i++;

                        if ($inner == 'new_products') { ?>
                            <div id="new_products" class="tab-content-item <?php if ($i == 1) {
                                                                                echo 'active';
                                                                            } ?>">
                                <?php $args = [
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    // 'post__in' => $kq,
                                    'order' => 'desc',
                                    'orderby' => 'date',
                                    'posts_per_page' => $attributes['post_number'],
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-products" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post();
        
                                            global $product; ?>

                                            <div class="item">
                                                <article class="post-box">
                                                    <?php if (has_post_thumbnail()) { ?>
                                                        <div class="post-image">
                                                            <a href="<?php the_permalink(); ?>" title="" class="image">
                                                                <img src="<?php echo lth_custom_img('full', 91, 186); ?>" width="91" height="186" alt="<?php the_title(); ?>">
                                                            </a>

                                                            <?php
                                                            $regular_price = $product->get_regular_price();
                                                            $sale_price = $product->get_sale_price();
                                                            ?>

                                                            <?php if ($sale_price) {
                                                                $sale = ($regular_price - $sale_price) * 100 / $regular_price;
                                                            ?>
                                                                <span class="on-sale"><?php echo round($sale) . '%'; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                                <?php the_title(); ?>
                                                            </a>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?> 

                                                        <?php get_template_part('woocommerce/loop/rating', ''); ?>

                                                        <div class="post-price">
                                                            <?php if (!$product->get_regular_price()) { ?>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <?php echo __('Liên hệ'); ?>
                                                                </span>
                                                            <?php } else {
                                                                echo $product->get_price_html();
                                                            } ?>
                                                        </div>

                                                        <div class="post-button">
                                                            <a class="btn btn-read-more" href="<?php the_permalink(); ?>">
                                                                <span><?php echo __('Xem chi tiết'); ?></span>
                                                            </a>

                                                            <?php if (!$product->is_type('variable')) {
                                                                echo apply_filters(
                                                                    'woocommerce_loop_add_to_cart_link',
                                                                    sprintf(
                                                                        '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" 
                                                                class="%s btn ajax_add_to_cart"><i class="fal fa-shopping-cart icon"></i><span>Thêm vào giỏ</span></a>',
                                                                        esc_url($product->add_to_cart_url()),
                                                                        esc_attr($product->id),
                                                                        esc_attr($product->get_sku()),
                                                                        $product->is_purchasable() ? 'add_to_cart_button' : '',
                                                                        esc_attr($product->product_type),
                                                                        esc_html($product->add_to_cart_text())
                                                                    ),
                                                                    $product
                                                                );
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>

                        <?php if ($inner == 'sale_products') { ?>
                            <div id="sale_products" class="tab-content-item <?php if ($i == 1) {
                                                                                echo 'active';
                                                                            } ?>">
                                <?php $args = [
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'posts_per_page' => $attributes['post_number'],
                                    'meta_query'     => array(
                                        'relation' => 'OR',
                                        array(
                                            'key'           => '_sale_price',
                                            'value'         => 0,
                                            'compare'       => '>',
                                            'type'          => 'numeric'
                                        )
                                    )
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-products" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post();
        
                                            global $product; ?>

                                            <div class="item">
                                                <article class="post-box">
                                                    <?php if (has_post_thumbnail()) { ?>
                                                        <div class="post-image">
                                                            <a href="<?php the_permalink(); ?>" title="" class="image">
                                                                <img src="<?php echo lth_custom_img('full', 91, 186); ?>" width="91" height="186" alt="<?php the_title(); ?>">
                                                            </a>

                                                            <?php
                                                            $regular_price = $product->get_regular_price();
                                                            $sale_price = $product->get_sale_price();
                                                            ?>

                                                            <?php if ($sale_price) {
                                                                $sale = ($regular_price - $sale_price) * 100 / $regular_price;
                                                            ?>
                                                                <span class="on-sale"><?php echo round($sale) . '%'; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                                <?php the_title(); ?>
                                                            </a>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?> 

                                                        <?php get_template_part('woocommerce/loop/rating', ''); ?>

                                                        <div class="post-price">
                                                            <?php if (!$product->get_regular_price()) { ?>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <?php echo __('Liên hệ'); ?>
                                                                </span>
                                                            <?php } else {
                                                                echo $product->get_price_html();
                                                            } ?>
                                                        </div>

                                                        <div class="post-button">
                                                            <a class="btn btn-read-more" href="<?php the_permalink(); ?>">
                                                                <span><?php echo __('Xem chi tiết'); ?></span>
                                                            </a>

                                                            <?php if (!$product->is_type('variable')) {
                                                                echo apply_filters(
                                                                    'woocommerce_loop_add_to_cart_link',
                                                                    sprintf(
                                                                        '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" 
                                                                class="%s btn ajax_add_to_cart"><i class="fal fa-shopping-cart icon"></i><span>Thêm vào giỏ</span></a>',
                                                                        esc_url($product->add_to_cart_url()),
                                                                        esc_attr($product->id),
                                                                        esc_attr($product->get_sku()),
                                                                        $product->is_purchasable() ? 'add_to_cart_button' : '',
                                                                        esc_attr($product->product_type),
                                                                        esc_html($product->add_to_cart_text())
                                                                    ),
                                                                    $product
                                                                );
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>

                        <?php if ($inner == 'featured_products') { ?>
                            <div id="featured_products" class="tab-content-item <?php if ($i == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                <?php $kq = array();
                                foreach ($attributes['products'] as $inner) {
                                    $post = $inner['product'];
                                    $kq[] = $post;
                                }

                                $args = [
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'post__in' => $kq,
                                    'orderby' => "post__in",
                                    'posts_per_page' => $attributes['post_number'],
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-products" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post();
        
                                            global $product; ?>

                                            <div class="item">
                                                <article class="post-box">
                                                    <?php if (has_post_thumbnail()) { ?>
                                                        <div class="post-image">
                                                            <a href="<?php the_permalink(); ?>" title="" class="image">
                                                                <img src="<?php echo lth_custom_img('full', 91, 186); ?>" width="91" height="186" alt="<?php the_title(); ?>">
                                                            </a>

                                                            <?php
                                                            $regular_price = $product->get_regular_price();
                                                            $sale_price = $product->get_sale_price();
                                                            ?>

                                                            <?php if ($sale_price) {
                                                                $sale = ($regular_price - $sale_price) * 100 / $regular_price;
                                                            ?>
                                                                <span class="on-sale"><?php echo round($sale) . '%'; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                                <?php the_title(); ?>
                                                            </a>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?> 

                                                        <?php get_template_part('woocommerce/loop/rating', ''); ?>

                                                        <div class="post-price">
                                                            <?php if (!$product->get_regular_price()) { ?>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <?php echo __('Liên hệ'); ?>
                                                                </span>
                                                            <?php } else {
                                                                echo $product->get_price_html();
                                                            } ?>
                                                        </div>

                                                        <div class="post-button">
                                                            <a class="btn btn-read-more" href="<?php the_permalink(); ?>">
                                                                <span><?php echo __('Xem chi tiết'); ?></span>
                                                            </a>

                                                            <?php if (!$product->is_type('variable')) {
                                                                echo apply_filters(
                                                                    'woocommerce_loop_add_to_cart_link',
                                                                    sprintf(
                                                                        '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" 
                                                                class="%s btn ajax_add_to_cart"><i class="fal fa-shopping-cart icon"></i><span>Thêm vào giỏ</span></a>',
                                                                        esc_url($product->add_to_cart_url()),
                                                                        esc_attr($product->id),
                                                                        esc_attr($product->get_sku()),
                                                                        $product->is_purchasable() ? 'add_to_cart_button' : '',
                                                                        esc_attr($product->product_type),
                                                                        esc_html($product->add_to_cart_text())
                                                                    ),
                                                                    $product
                                                                );
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>

                        <?php if ($inner == 'random_products') { ?>
                            <div id="random_products" class="tab-content-item <?php if ($i == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                <?php $args = [
                                    'post_type' => 'product',
                                    'post_status' => 'publish',
                                    'orderby' => 'rand',
                                    'posts_per_page' => $attributes['post_number'],
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-products" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post();
        
                                            global $product; ?>

                                            <div class="item">
                                                <article class="post-box">
                                                    <?php if (has_post_thumbnail()) { ?>
                                                        <div class="post-image">
                                                            <a href="<?php the_permalink(); ?>" title="" class="image">
                                                                <img src="<?php echo lth_custom_img('full', 91, 186); ?>" width="91" height="186" alt="<?php the_title(); ?>">
                                                            </a>

                                                            <?php
                                                            $regular_price = $product->get_regular_price();
                                                            $sale_price = $product->get_sale_price();
                                                            ?>

                                                            <?php if ($sale_price) {
                                                                $sale = ($regular_price - $sale_price) * 100 / $regular_price;
                                                            ?>
                                                                <span class="on-sale"><?php echo round($sale) . '%'; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                                <?php the_title(); ?>
                                                            </a>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?> 

                                                        <?php get_template_part('woocommerce/loop/rating', ''); ?>

                                                        <div class="post-price">
                                                            <?php if (!$product->get_regular_price()) { ?>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <?php echo __('Liên hệ'); ?>
                                                                </span>
                                                            <?php } else {
                                                                echo $product->get_price_html();
                                                            } ?>
                                                        </div>

                                                        <div class="post-button">
                                                            <a class="btn btn-read-more" href="<?php the_permalink(); ?>">
                                                                <span><?php echo __('Xem chi tiết'); ?></span>
                                                            </a>

                                                            <?php if (!$product->is_type('variable')) {
                                                                echo apply_filters(
                                                                    'woocommerce_loop_add_to_cart_link',
                                                                    sprintf(
                                                                        '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" 
                                                                class="%s btn ajax_add_to_cart"><i class="fal fa-shopping-cart icon"></i><span>Thêm vào giỏ</span></a>',
                                                                        esc_url($product->add_to_cart_url()),
                                                                        esc_attr($product->id),
                                                                        esc_attr($product->get_sku()),
                                                                        $product->is_purchasable() ? 'add_to_cart_button' : '',
                                                                        esc_attr($product->product_type),
                                                                        esc_html($product->add_to_cart_text())
                                                                    ),
                                                                    $product
                                                                );
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>

                        <?php if ($inner == 'viewed_products') { ?>
                            <div id="viewed_products" class="tab-content-item <?php if ($i == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                <?php if (isset($_SESSION["viewed"]) && $_SESSION["viewed"]) {
                                    $data = $_SESSION["viewed"];
                                    $args = [
                                        'post_type' => 'product',
                                        'post_status' => 'publish',
                                        'post__in' => $data,
                                        'orderby' => 'rand',
                                        'posts_per_page' => $attributes['post_number'],
                                    ];

                                    $getposts = new WP_query($args);
                                    global $wp_query;
                                    $wp_query->in_the_loop = true; ?>
                                    <div class="swiper swiper-slider swiper-products" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($getposts->have_posts()) : $getposts->the_post();
                                            global $product; ?>

                                            <div class="item">
                                                <article class="post-box">
                                                    <?php if (has_post_thumbnail()) { ?>
                                                        <div class="post-image">
                                                            <a href="<?php the_permalink(); ?>" title="" class="image">
                                                                <img src="<?php echo lth_custom_img('full', 91, 186); ?>" width="91" height="186" alt="<?php the_title(); ?>">
                                                            </a>

                                                            <?php
                                                            $regular_price = $product->get_regular_price();
                                                            $sale_price = $product->get_sale_price();
                                                            ?>

                                                            <?php if ($sale_price) {
                                                                $sale = ($regular_price - $sale_price) * 100 / $regular_price;
                                                            ?>
                                                                <span class="on-sale"><?php echo round($sale) . '%'; ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                                <?php the_title(); ?>
                                                            </a>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?> 

                                                        <?php get_template_part('woocommerce/loop/rating', ''); ?>

                                                        <div class="post-price">
                                                            <?php if (!$product->get_regular_price()) { ?>
                                                                <span class="woocommerce-Price-amount amount">
                                                                    <?php echo __('Liên hệ'); ?>
                                                                </span>
                                                            <?php } else {
                                                                echo $product->get_price_html();
                                                            } ?>
                                                        </div>

                                                        <div class="post-button">
                                                            <a class="btn btn-read-more" href="<?php the_permalink(); ?>">
                                                                <span><?php echo __('Xem chi tiết'); ?></span>
                                                            </a>

                                                            <?php if (!$product->is_type('variable')) {
                                                                echo apply_filters(
                                                                    'woocommerce_loop_add_to_cart_link',
                                                                    sprintf(
                                                                        '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" 
                                                                class="%s btn ajax_add_to_cart"><i class="fal fa-shopping-cart icon"></i><span>Thêm vào giỏ</span></a>',
                                                                        esc_url($product->add_to_cart_url()),
                                                                        esc_attr($product->id),
                                                                        esc_attr($product->get_sku()),
                                                                        $product->is_purchasable() ? 'add_to_cart_button' : '',
                                                                        esc_attr($product->product_type),
                                                                        esc_html($product->add_to_cart_text())
                                                                    ),
                                                                    $product
                                                                );
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        <?php endwhile;
                                        // reset post data
                                        wp_reset_postdata(); ?>
                                    </div>
                                <?php } else {
                                    get_template_part('template-parts/content', 'none');
                                } ?>
                            </div>
                    <?php }
                    endforeach; ?>
                </div>

                <?php if ($attributes['button_url']) : ?>
                    <div class="module_button">
                        <a href="<?php echo esc_url($attributes['button_url']); ?>">
                            <?php echo $attributes['button_text']; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </article>
<?php
        return ob_get_clean();
    }
endif;
?>