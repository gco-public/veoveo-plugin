<?php

/**
 * @block-slug  :   lth-services-category
 * @block-output:   lth_services_category_output
 * @block-attributes: get from attributes.php
 */

// filter for Frontend output.
add_filter('lazyblock/lth-services-category/frontend_callback', 'lth_services_category_output_fe', 10, 2);

if (!function_exists('lth_services_category_output_fe')) :
    /**
     * Test Render Callback
     *
     * @param string $output - block output.
     * @param array  $attributes - block attributes.
     */
    function lth_services_category_output_fe($output, $attributes)
    {
        ob_start();
?>
        <article class="lth-services <?php echo $attributes['class']; ?>">
            <div class="module module_services">
                <?php if ($attributes['title'] || $attributes['description'] || $attributes['show_items']) : ?>
                    <div class="module_header title-box title-align-<?php echo $attributes['title_align']; ?>">
                        <?php if ($attributes['title']) : ?>
                            <h2 class="title">
                                <?php if ($attributes['title_url']) : ?>
                                    <a href="<?php echo esc_url($attributes['title_url']); ?>" title="">
                                    <?php endif; ?>
                                    <?php echo wpautop(esc_html($attributes['title'])); ?>
                                    <?php if ($attributes['title_url']) : ?>
                                    </a>
                                <?php endif; ?>
                            </h2>
                        <?php endif; ?>

                        <?php if ($attributes['description']) : ?>
                            <div class="infor">
                                <?php echo wpautop(esc_html($attributes['description'])); ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($attributes['show_items']) : ?>
                            <div class="cat-list">
                                <ul>
                                    <?php foreach ($attributes['items'] as $inner) { ?>
                                        <li>
                                            <a href="<?php echo get_category_link($inner['item']); ?>" data_id="<?php echo $inner['item']; ?>"><?php echo get_cat_name($inner['item']); ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <div class="module_content content_text_<?php echo $attributes['text_align']; ?> module_style_<?php echo $attributes['style']; ?>">
                    <?php
                    if (isset($attributes['featured_posts'])) {
                        $kq = array();
                        foreach ($attributes['posts'] as $inner) {
                            $post = $inner['post'];
                            $kq[] = $post;
                        }

                        $args = [
                            'post_type' => 'service',
                            'post_status' => 'publish',
                            'post__in' => $kq,
                            'orderby' => "post__in",
                            'posts_per_page' => $attributes['post_number'],
                        ];
                    } else {
                        $i = 0;
                        foreach ($attributes['items'] as $inner) {
                            $i++;
                            if ($i == '1') {
                                $cat = $inner['item'];
                            }
                        }

                        $args = [
                            'post_type' => 'service',
                            'post_status' => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'service_cat',
                                    'field'    => 'id',
                                    'terms'    => $cat,
                                ),
                            ),
                            'posts_per_page' => $attributes['post_number'],
                            'orderby' => $attributes['orderby'],
                            'order' => $attributes['order'],
                        ];
                    }
                    $wp_query = new WP_Query($args);
                    if ($wp_query->have_posts()) { ?>
                        <div class="swiper swiper-slider swiper-services" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                            <?php while ($wp_query->have_posts()) {
                                $wp_query->the_post(); ?>

                                <div class="item">
                                    <div class="post-box">
                                        <?php if (has_post_thumbnail()) { ?>
                                            <div class="post-image">
                                                <a href="<?php the_permalink(); ?>" title="" class="image">
                                                    <img src="<?php echo lth_custom_img('full', 333, 230); ?>" width="333" height="230" alt="<?php the_title(); ?>">
                                                </a>
                                            </div>
                                        <?php } ?>

                                        <div class="post-content">
                                            <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                <h1 class="post-name">
                                            <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                <h2 class="post-name">
                                            <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                <h3 class="post-name">
                                            <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                <h4 class="post-name">
                                            <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                <h5 class="post-name">
                                            <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                <h6 class="post-name">
                                            <?php } ?>
                                                <a href="<?php the_permalink(); ?>" title="">
                                                    <?php the_title(); ?>
                                                </a>
                                            <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                </h1>
                                            <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                </h2>
                                            <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                </h3>
                                            <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                </h4>
                                            <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                </h5>
                                            <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                </h6>
                                            <?php } ?>                                                        

                                            <p class="content-days">
                                                <?php the_time('d/m/Y'); ?>
                                            </p>

                                            <div class="post-excerpt">
                                                <?php wpautop(the_excerpt()); ?>
                                            </div>

                                            <div class="post-button">
                                                <a href="<?php the_permalink(); ?>" class="btn" title="">
                                                    <?php echo __('Xem thêm'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <?php if ($attributes['button_text']) : ?>
                            <div class="module_button">
                                <a href="<?php echo esc_url($attributes['button_url']); ?>" class="btn">
                                    <?php echo esc_html($attributes['button_text']); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php } else {
                        get_template_part('template-parts/content', 'none');
                    }
                    // reset post data
                    wp_reset_postdata();
                    ?>
                </div>

                <?php if ($attributes['button_url']) : ?>
                    <div class="module_button">
                        <a href="<?php echo esc_url($attributes['button_url']); ?>">
                            <?php echo $attributes['button_text']; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </article>
<?php
        return ob_get_clean();
    }
endif;
?>