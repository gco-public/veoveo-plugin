<?php

/**
 * @block-slug  :   lth-testimonials
 * @block-output:   lth_testimonials_output
 * @block-attributes: get from attributes.php
 */

// filter for Frontend output.
add_filter('lazyblock/lth-testimonials/frontend_callback', 'lth_testimonials_output_fe', 10, 2);

if (!function_exists('lth_testimonials_output_fe')) :
    /**
     * Test Render Callback
     *
     * @param string $output - block output.
     * @param array  $attributes - block attributes.
     */
    function lth_testimonials_output_fe($output, $attributes)
    {
        ob_start();
?>
        <article class="lth-testimonials <?php echo $attributes['class']; ?>">
            <div class="module module_testimonials">
                <?php if ($attributes['title'] || $attributes['description'] || $attributes['show_items']) : ?>
                    <div class="module_header title-box title-align-<?php echo $attributes['title_align']; ?>">
                        <?php if ($attributes['title']) : ?>
                            <h2 class="title">
                                <?php if ($attributes['title_url']) : ?>
                                    <a href="<?php echo esc_url($attributes['title_url']); ?>" title="">
                                    <?php endif; ?>
                                    <?php echo wpautop(esc_html($attributes['title'])); ?>
                                    <?php if ($attributes['title_url']) : ?>
                                    </a>
                                <?php endif; ?>
                            </h2>
                        <?php endif; ?>

                        <?php if ($attributes['description']) : ?>
                            <div class="infor">
                                <?php echo wpautop(esc_html($attributes['description'])); ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($attributes['show_items']) : ?>
                            <div class="cat-list tab-list">
                                <ul>
                                    <?php $j = 0;
                                    foreach ($attributes['select'] as $inner) :
                                        $j++;

                                        if ($inner == 'new_posts') { ?>
                                            <li>
                                                <a href="#new_testimonials" class="<?php if ($j == 1) {
                                                                                echo 'active';
                                                                            } ?>">
                                                    <?php echo __('Bài viết mới'); ?>
                                                </a>
                                            </li>
                                        <?php }
                                        if ($inner == 'random_posts') { ?>
                                            <li>
                                                <a href="#random_testimonials" class="<?php if ($j == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                                    <?php echo __('Bài viết ngẫu nhiên'); ?>
                                                </a>
                                            </li>
                                        <?php }
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <div class="module_content content_text_<?php echo $attributes['text_align']; ?> tab-content module_style_<?php echo $attributes['style']; ?>">
                    <?php $i = 0;
                    foreach ($attributes['select'] as $inner) :
                        $i++;

                        if ($inner == 'new_posts') { ?>
                            <div id="new_testimonials" class="tab-content-item <?php if ($i == 1) {
                                                                            echo 'active';
                                                                        } ?>">
                                <?php $args = [
                                    'post_type' => 'testimonial',
                                    'post_status' => 'publish',
                                    'posts_per_page' => $attributes['post_number'],
                                    'orderby' => $attributes['orderby'],
                                    'order' => $attributes['order'],
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-testimonials" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post(); ?>

                                            <div class="item">
                                                <div class="post-box">
                                                    <div class="post-image">
                                                        <a href="#" title="" class="image">
                                                            <img src="<?php the_field('avata'); ?>" width="333" height="230" alt="<?php the_title(); ?>">
                                                        </a>
                                                    </div>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <?php the_field('name'); ?>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?>   
                                                        
                                                        <div class="post-job">
                                                            <?php echo wpautop(the_field('job')); ?>
                                                        </div>

                                                        <div class="post-excerpt">
                                                            <?php wpautop(the_field('excerpt')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>

                        <?php if ($inner == 'featured_posts') { ?>
                            <div id="featured_blogs" class="tab-content-item <?php if ($i == 1) {
                                                                                    echo 'active';
                                                                                } ?>">
                                <?php $kq = array();
                                foreach ($attributes['posts'] as $inner) {
                                    $post = $inner['post'];
                                    $kq[] = $post;
                                }

                                $args = [
                                    'post_type' => 'testimonial',
                                    'post_status' => 'publish',
                                    'post__in' => $kq,
                                    'orderby' => "post__in",
                                    'posts_per_page' => $attributes['post_number'],
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-blogs" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post(); ?>

                                            <div class="item">
                                                <div class="post-box">
                                                    <div class="post-image">
                                                        <a href="#" title="" class="image">
                                                            <img src="<?php the_field('avata'); ?>" width="333" height="230" alt="<?php the_title(); ?>">
                                                        </a>
                                                    </div>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <?php the_field('name'); ?>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?>   
                                                        
                                                        <div class="post-job">
                                                            <?php echo wpautop(the_field('job')); ?>
                                                        </div>

                                                        <div class="post-excerpt">
                                                            <?php wpautop(the_field('excerpt')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>

                        <?php if ($inner == 'random_posts') { ?>
                            <div id="random_testimonials" class="tab-content-item <?php if ($i == 1) {
                                                                                echo 'active';
                                                                            } ?>">
                                <?php $args = [
                                    'post_type' => 'testimonial',
                                    'post_status' => 'publish',
                                    'posts_per_page' => $attributes['post_number'],
                                    'orderby' => 'rand',
                                ];

                                $wp_query = new WP_Query($args);
                                if ($wp_query->have_posts()) { ?>
                                    <div class="swiper swiper-slider swiper-testimonials" data-item="<?php echo $attributes['item']; ?>" data-item_lg="<?php echo $attributes['item_lg']; ?>" data-item_md="<?php echo $attributes['item_md']; ?>" data-item_sm="<?php echo $attributes['item_sm']; ?>" data-item_mb="<?php echo $attributes['item_mb']; ?>" data-row="<?php echo $attributes['item_row']; ?>" data-dots="<?php echo $attributes['item_dots']; ?>" data-arrows="<?php echo $attributes['item_arrows']; ?>" data-vertical="<?php echo $attributes['item_vertical']; ?>" data-autoplay="<?php echo $attributes['item_autoplay']; ?>">
                                        <?php while ($wp_query->have_posts()) {
                                            $wp_query->the_post(); ?>

                                            <div class="item">
                                                <div class="post-box">
                                                    <div class="post-image">
                                                        <a href="#" title="" class="image">
                                                            <img src="<?php the_field('avata'); ?>" width="333" height="230" alt="<?php the_title(); ?>">
                                                        </a>
                                                    </div>

                                                    <div class="post-content">
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            <h1 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            <h2 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            <h3 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            <h4 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            <h5 class="post-name">
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            <h6 class="post-name">
                                                        <?php } ?>
                                                            <?php the_field('name'); ?>
                                                        <?php if ($attributes['post_name_tag'] == 'h1') { ?>
                                                            </h1>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h2') { ?>
                                                            </h2>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h3') { ?>
                                                            </h3>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h4') { ?>
                                                            </h4>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h5') { ?>
                                                            </h5>
                                                        <?php } elseif ($attributes['post_name_tag'] == 'h6') { ?>
                                                            </h6>
                                                        <?php } ?>   
                                                        
                                                        <div class="post-job">
                                                            <?php echo wpautop(the_field('job')); ?>
                                                        </div>

                                                        <div class="post-excerpt">
                                                            <?php wpautop(the_field('excerpt')); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php
                                } else {
                                    get_template_part('template-parts/content', 'none');
                                }
                                // reset post data
                                wp_reset_postdata(); ?>
                            </div>
                        <?php } ?>
                    <?php endforeach; ?>
                </div>

                <?php if ($attributes['button_url']) : ?>
                    <div class="module_button">
                        <a href="<?php echo esc_url($attributes['button_url']); ?>">
                            <?php echo $attributes['button_text']; ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </article>
<?php
        return ob_get_clean();
    }
endif;
?>