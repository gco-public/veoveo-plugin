<?php

/*
    Plugin Name: LTH Blocks
    Plugin URI: https://themeforest.net/
    Description: LTH Widgets
    Version: 1.0.1
    Author: LTH Design Team
    Author URI: https://wordpress.com/wordpress-plugins/
    Text Domain: lth-theme
*/

// Exit if accessed directly.
defined('ABSPATH') || exit;

///////////////////////////////////

foreach (glob(get_template_directory() . '/blocks/lazyblock-*/*.php') as $file) {
    require_once($file);
}

foreach (glob(get_template_directory() . '/blocks/controls/lazyblock-*/*.php') as $file) {
    require_once($file);
}

foreach (glob(plugin_dir_path(__FILE__) . '/controls/lazyblock-*/*.php') as $file) {
    require_once($file);
}