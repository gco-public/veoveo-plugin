<?php

require plugin_dir_path(__FILE__) . 'post-types/html-blocks.php';

require plugin_dir_path(__FILE__) . 'post-types/projects.php';

require plugin_dir_path(__FILE__) . 'post-types/services.php';

require plugin_dir_path(__FILE__) . 'post-types/teams.php';

require plugin_dir_path(__FILE__) . 'post-types/testimonials.php';
