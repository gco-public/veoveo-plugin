<?php

add_action( 'init', 'create_testimonials_item' );

function create_testimonials_item() {
	$veoveo = get_field('veoveo', 'option');
	$show_testimonials = $veoveo['show_testimonials'];

	if ($show_testimonials == 'yes') {
		$labels = array(
			'name' => _x('Khách Hàng', 'post type general name'),
			'singular_name' => _x('Khách Hàng', 'post type singular name'),
			'add_new' => _x('Thêm Mới', 'Testimonials'),
			'add_new_item' => __('Thêm Mới'),
			'edit_item' => __('Sửa'),
			'new_item' => __('Thêm Mới'),
			'all_items' => __('Tất Cả'),
			'view_item' => __('Xem'),
			'search_items' => __('Tìm Kiếm'),
			'not_found' =>  __('Không Tìm Thấy'),
			'not_found_in_trash' => __('Thùng Rác Rỗng'), 
			'parent_item_colon' => '',
			'menu_name' => 'Khách Hàng'
		);
	
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'show_in_nav_menus' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'has_archive' => true, 
			'hierarchical' => true,
			'menu_position' => 4,	
			'supports'            => array(
				'title',
				// 'excerpt',
				// 'editor',
				// 'thumbnail',
			),
			'show_in_rest' => true,
			'rewrite' => array('slug' => 'testimonial'),
			'with_front' => FALSE,
			'menu_icon' => 'dashicons-welcome-widgets-menus',
	
		);
	
		register_post_type('testimonial',$args);
	}
}