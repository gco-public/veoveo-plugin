<?php

add_action( 'init', 'create_services_item' );

function create_services_item() {
	$veoveo = get_field('veoveo', 'option');
	$show_services = $veoveo['show_services'];

	if ($show_services == 'yes') {
		$labels = array(
			'name' => _x('Dịch Vụ', 'post type general name'),
			'singular_name' => _x('Dịch Vụ', 'post type singular name'),
			'add_new' => _x('Thêm Mới', 'Testimonials'),
			'add_new_item' => __('Thêm Mới'),
			'edit_item' => __('Sửa'),
			'new_item' => __('Thêm Mới'),
			'all_items' => __('Tất Cả'),
			'view_item' => __('Xem'),
			'search_items' => __('Tìm Kiếm'),
			'not_found' =>  __('Không Tìm Thấy'),
			'not_found_in_trash' => __('Thùng Rác Rỗng'), 
			'parent_item_colon' => '',
			'menu_name' => 'Dịch Vụ'
		);
	
		$args = array(
			'labels' => $labels,
			'supports' => array(),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'post',
			'menu_icon' => 'dashicons-welcome-widgets-menus',
		);	
		register_post_type('service',$args);

		register_taxonomy('service_cat',array('service'), array(
			'hierarchical' => true,
			'labels' => 'Chuyên Mục',
			'show_ui' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'service_cat' ),
		));
	}
}