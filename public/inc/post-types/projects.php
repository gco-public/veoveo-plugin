<?php

add_action( 'init', 'create_projects_item' );

function create_projects_item() {
	$veoveo = get_field('veoveo', 'option');
	$show_projects = $veoveo['show_projects'];

	if ($show_projects == 'yes') {
		$labels = array(
			'name' => _x('Dự Án', 'post type general name'),
			'singular_name' => _x('Dự Án', 'post type singular name'),
			'add_new' => _x('Thêm Mới', 'Testimonials'),
			'add_new_item' => __('Thêm Mới'),
			'edit_item' => __('Sửa'),
			'new_item' => __('Thêm Mới'),
			'all_items' => __('Tất Cả'),
			'view_item' => __('Xem'),
			'search_items' => __('Tìm Kiếm'),
			'not_found' =>  __('Không Tìm Thấy'),
			'not_found_in_trash' => __('Thùng Rác Rỗng'), 
			'parent_item_colon' => '',
			'menu_name' => 'Dự Án'
		);
	
		$args = array(
			'labels' => $labels,
			'supports' => array(),
			// 'taxonomies' => array('project_cat'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'post',
			'menu_icon' => 'dashicons-welcome-widgets-menus',
		);	
		register_post_type('project',$args);

		register_taxonomy('project_cat',array('project'), array(
			'hierarchical' => true,
			'labels' => 'Chuyên Mục',
			'show_ui' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'project_cat' ),
		));
	}
}